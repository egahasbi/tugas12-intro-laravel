<!DOCTYPE html>
<html>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
      <label>First name:</label><br><br>
      <input type="text" name="firstname"/><br><br>
      <label>Last name:</label><br><br>
      <input type="text" name="lastname"/><br><br>
      <label>Gender:</label><br><br>
      <input type="radio" name="gender"/>Male<br>
      <input type="radio" name="gender"/>Female<br>
      <input type="radio" name="gender"/>Other<br><br>
      <label>Nationality:</label><br><br>
      <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Singaporean">Singaporean</option>
      </select><br><br>
      <label>Language Spoken:</label><br><br>
      <input type="checkbox" name="language"/>Bahasa Indonesia<br>
      <input type="checkbox" name="language"/>English<br>
      <input type="checkbox" name="language"/>Other<br><br>
      <label>Bio:</label><br><br>
      <textarea name="bio" rows="10" cols="30"></textarea><br>
      <input type="submit" value="Sign Up"/>
    </form>
  </body>
</html>
